# calendar_Documentation

Role:

- Gagatek - Back-end + Salt
- Hofbauer - Front-end

- Gaładyk & Gierczak & Pabin (+ ew. Gagatek/Hofbauer) - dokumentacja specyfikacji
- Zadurski & Kleszczonek - dokumentacja testowa
- Kasak & Najda - dokumentacja systemu
- Lech & Bruzdewicz - dokumentacja eksploatacyjna


Utworzyliśmy 3 repozytoria:

- calendar_Documentation - dokumentacja aplikacji
- calendar_API - interfejs wraz z backendem aplikacji
- calendar_APP - aplikacja wraz z frontendem